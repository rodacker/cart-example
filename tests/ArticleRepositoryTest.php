<?php

namespace Rodacker\CartExample\Test;

use PHPUnit\Framework\TestCase;
use Rodacker\CartExample\Article;
use Rodacker\CartExample\ArticleRepository;
use Rodacker\CartExample\ArticleLoader;

/**
 * Class ArticleRepositoryTest
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class ArticleRepositoryTest extends TestCase
{

    /**
     * @test
     */
    public function createNewRepository()
    {

        $repository = new ArticleRepository(new ArticleLoader());
        $articles   = $repository->getArticles();

        $this->assertTrue(is_array($articles));
        $this->assertEquals(9, count($articles));
    }

    /**
     * @test
     */
    public function getArticle()
    {

        $repository = new ArticleRepository(new ArticleLoader());
        $article    = $repository->getArticle(
            'this id does not exist in the repository'
        );
        $this->assertFalse($article);

        $article = $repository->getArticle('article-1');
        $this->assertInstanceOf(Article::class, $article);
    }

}