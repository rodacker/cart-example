<?php

namespace Rodacker\CartExample\Test;

use PHPUnit\Framework\TestCase;
use Rodacker\CartExample\Article;
use Rodacker\CartExample\ArticleRepository;
use Rodacker\CartExample\ArticleLoader;

/**
 * Class ArticleRepositoryTest
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class ArticleLoaderTest extends TestCase
{

    /**
     * @test
     */
    public function loadArticles()
    {
        $loader = new ArticleLoader();
        $articles = $loader->load();

        $this->assertTrue(is_array($articles));
        $this->assertEquals(9, count($articles));
    }

    /**
     * @test
     */
    public function emptyJsonError()
    {
        $stub = $this->getMockBuilder(ArticleLoader::class)
            ->setMethods(['loadJsonFile'])
            ->getMock();

        $stub->method('loadJsonFile')->willReturn(null);
        
        $this->expectException(\Exception::class);
        new ArticleRepository($stub);
    }

}