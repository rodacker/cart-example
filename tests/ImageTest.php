<?php

namespace Rodacker\CartExample\Test;

use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Item\CartItemInterface;
use Rodacker\CartExample\Article;
use Rodacker\CartExample\Image;

class ImageTest extends TestCase
{

    /**
     * @test
     */
    public function createImage()
    {

        // create image object
        $relativePathname = 'img/200.png';
        $absolutePathname = 'https://via.placeholder.com/200x150';
        $image = new Image($relativePathname, $absolutePathname);

        $this->assertEquals($relativePathname, $image->getRelativePathname());
        $this->assertEquals($absolutePathname, $image->getAbsolutePathname());
    }
}