<?php

namespace Rodacker\CartExample\Test;

use PHPUnit\Framework\TestCase;
use Rodacker\Cart\Item\CartItemInterface;
use Rodacker\CartExample\Article;
use Rodacker\CartExample\Image;

/**
 * Class ArticleTest
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class ArticleTest extends TestCase
{

    /**
     * @test
     */
    public function createNewArticle()
    {

        // create image object
        $relativePathname = 'img/200.png';
        $absolutePathname = 'https://via.placeholder.com/200x150';
        $images = [
            new Image($relativePathname, $absolutePathname),
        ];


        // create article
        $article = new Article('an article', 4000, $images);

        $this->assertInstanceOf(CartItemInterface::class, $article);

        $this->assertEquals('an article', $article->getName());
        $this->assertTrue(
            is_string($article->getId()) or is_numeric($article->getId())
        );

        $this->assertEquals(4000, $article->getPrice());
        $this->assertTrue(is_array($article->getImages()));
        $this->assertEquals($relativePathname, $article->getImagePath());
    }
}