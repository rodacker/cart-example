<?php

require_once __DIR__.'/bootstrap.php';

use Rodacker\Cart\Cart;
use Rodacker\Cart\Item\CartItem;
use Rodacker\Cart\Identifier\Cookie;
use Rodacker\Cart\Storage\SessionStorage;
use Rodacker\CartExample;
use Michelf\Markdown;
use Michelf\MarkdownExtra;

use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\RedirectResponse;

/**
 *  --------------  init & config --------------
 */
$app = new Silex\Application();
Request::enableHttpMethodParameterOverride();
$app['debug'] = true;


/**
 *  --------------  services --------------
 */
$app->register(
    new Silex\Provider\TwigServiceProvider(),
    [
        'twig.path' => __DIR__.'/Resources/views',
    ]
);

$app->register(
    new Silex\Provider\AssetServiceProvider(),
    [
        'assets.version'        => 'v1',
        'assets.version_format' => '%s?version=%s',
        'assets.named_packages' => [
            'css'    => [
                'version'   => 'css3',
                'base_path' => '/css'
            ],
        ],
    ]
);


/**
 *  --------------  repository & cart --------------
 */

// create a new article repository holding the articles
$repository = new CartExample\ArticleRepository(new CartExample\ArticleLoader());

// create a new cart object
$cart = new Cart(new Cookie(), new SessionStorage());


/**
 *  --------------  routes --------------
 */
$app->get(
    '/',
    function () use ($app, $cart) {
        return $app['twig']->render('home.twig', ['cart' => $cart]);
    }
);

$app->get(
    '/shop',
    function () use ($app, $repository) {
        return $app['twig']->render('shop.twig', ['articles' => $repository->getArticles()]);
    }
);

$app->get(
    '/cart',
    function () use ($app, $cart) {
        return $app['twig']->render('cart.twig', ['cart' => $cart]);
    }
);

$app->post(
    '/cart',
    function (Request $request) use ($repository, $cart) {

        $article = $repository->getArticle($request->get('id'));
        $quantity = $request->get('quantity');

        $item = new CartItem($article, $article->getPrice(), $quantity);
        $cart->addItem($item);

        return new RedirectResponse('/cart');
    }
);

$app->delete(
    '/cart/item',
    function (Request $request) use ($cart) {

        $id = $request->get('id');
        $item = $cart->getItem($id);
        $cart->removeItem($item);

        return new RedirectResponse('/cart');
    }
);

$app->delete(
    '/cart',
    function () use ($cart) {
        $cart->clear();
        return new RedirectResponse('/cart');
    }
);

$app->get(
    '/documentation',
    function () use ($app) {

        $readme = file_get_contents(__DIR__.'/../README.md');
        $content = MarkdownExtra::defaultTransform($readme);

        return $app['twig']->render('documentation.twig', ['content' => $content]);
    }
);

return $app;