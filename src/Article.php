<?php

namespace Rodacker\CartExample;

use Rodacker\Cart\Item\CartImageInterface;
use Rodacker\Cart\Item\CartItemInterface;

/**
 * Class Article
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class Article implements CartItemInterface
{

    /**
     * @var
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $price;

    /**
     * @var CartImageInterface[]
     */
    private $images;

    /**
     * Article constructor.
     *
     * @param string               $name
     * @param int                  $price
     * @param CartImageInterface[] $images
     */
    public function __construct($name, $price, array $images)
    {
        $this->id     = uniqid('article-');

        // todo: add constraints
        $this->name   = $name;
        $this->price  = $price;
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return array|CartImageInterface[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return count($this->images) > 0 ? $this->images[0]->getRelativePathname(
        ) : 'https://via.placeholder.com/200';
    }
}