<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\CartExample;

/**
 * Class ArticleRepository
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class ArticleRepository
{

    const ARTICLES_JSON_PATHNAME = __DIR__.'/articles.json';

    /** @var  Article[] */
    private $articles;

    /**
     * ArticleRepository constructor.
     */
    public function __construct(ArticleLoader $loader)
    {
        $this->articles = $loader->load();
    }

    /**
     * @return Article[]
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @param $id
     *
     * @return bool|Article
     */
    public function getArticle($id)
    {
        if (array_key_exists($id, $this->articles)) {
            return $this->articles[$id];
        }

        return false;
    }

}