<?php

/**
 * This file is part of Rodacker Cart, a simple PHP cart package provinding
 * basic shopping cart features
 *
 * Copyright (c) 2017 Patrick Rodacker <patrick.rodacker@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @package   rodacker/cart
 * @author    Patrick Rodacker <patrick.rodacker@gmail.com>
 * @copyright 2017 Patrick Rodacker
 * @version   dev
 * @link      https://gitlab.com/rodacker/cart
 */

namespace Rodacker\CartExample;

use Rodacker\Cart\Item\CartImageInterface;

/**
 * Class Image
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class Image implements CartImageInterface
{

    /**
     * @var
     */
    private $relativePathname;

    /**
     * @var
     */
    private $absolutePathname;

    /**
     * Image constructor.
     *
     * @param $absolutePathname
     * @param $relativePathname
     */
    public function __construct($relativePathname, $absolutePathname)
    {
        $this->absolutePathname = $absolutePathname;
        $this->relativePathname = $relativePathname;
    }

    /**
     * @return mixed
     */
    public function getRelativePathname()
    {
        return $this->relativePathname;
    }

    /**
     * @return mixed
     */
    public function getAbsolutePathname()
    {
        return $this->absolutePathname;
    }

}