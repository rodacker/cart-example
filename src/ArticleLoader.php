<?php

namespace Rodacker\CartExample;

/**
 * Class ArticlesLoader
 *
 * @author Patrick Rodacker <patrick.rodacker@gmail.com>
 */
class ArticleLoader
{

    const ARTICLES_JSON_PATHNAME = __DIR__ . '/articles.json';

    /**
     * @return array
     * @throws \Exception
     */
    public function load()
    {
        $json = $this->loadJsonFile();

        if ( !$json) {
            throw new \Exception(
                sprintf(
                    'the articles json file %s is empty or does not contain any content.',
                    self::ARTICLES_JSON_PATHNAME
                )
            );
        }

        $articles = [];
        foreach ($json as $conf) {

            $images = [];
            foreach ($conf['images'] as $imageConf) {
                $images[] = new Image(
                    $imageConf['relativePathname'],
                    $imageConf['absolutePathname']
                );
            }

            $article = new Article($conf['name'], $conf['price'], $images);

            // override internal id via reflection
            $reflection = new \ReflectionClass($article);
            $property   = $reflection->getProperty('id');
            $property->setAccessible(true);
            $property->setValue($article, $conf['id']);

            $articles[$article->getId()] = $article;
        }

        return $articles;
    }

    /**
     * @return mixed
     */
    protected function loadJsonFile() {
        $str  = file_get_contents(self::ARTICLES_JSON_PATHNAME);
        return json_decode($str, true);
    }

}