# Rodacker Cart Example

This is an example implementation of the [rodacker/cart](https://gitlab.com/rodacker/cart) package
based on the [Silex](https://silex.sensiolabs.org/) framework.

## Install

### Overview

Run the following commands to get the example up and running

```
git clone https://gitlab.com/rodacker/cart-example.git
cd cart-example/
composer install
php -S localhost:8000 -t web/
```

After that open `http://localhost:8000 in your browser and explore.

### Details

Here is some detailed installation information:  

#### Getting the sources
Clone the project 

```
git clone https://gitlab.com/rodacker/cart.git
```

or download it from

https://gitlab.com/rodacker/cart/repository/archive.zip

#### Install dependencies

This package uses [Composer](https://getcomposer.org/) for package management. 

You can [install Composer](https://getcomposer.org/download/) with
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

Run the following command from your terminal:

```
composer install
```

This will only work if you have [installed composer globally](https://getcomposer.org/doc/00-intro.md#globally)


#### Web Server
The quickest way to view this example is using the PHP built-in web server, 
but of course you can also set up another local web server.  
 
Just make sure you configure your local webserver to point to the `web` folder.

#### PHP Built-in web server
As of PHP 5.4.0 the CLI SAPI provides a built-in web server

```bash
php -S localhost:8000 -t web/
```

#### Apache
 
```
<VirtualHost *:80>
    DocumentRoot /path/to/cart-example/web
    ServerName your-cart-example-domain 

    <Directory "/path/to/cart-example/web">
        AllowOverride All
    </Directory>
</VirtualHost>
```

## Project Structure

### Folder structure
```
app/                    
app/Resources/views     
src/                    
tests/                  
vendor/
web/
```

### Autoloader
The projects provides a simple boostrap file at `app/bootstrap.php` which 
loads the composer autoloader from `vendor/autoload.php`. 

### Application
The main application `app/app.php` currently performs the following tasks:
 
  * intialize Silex application
  * register `TwigServiceProvider` and `AssetServiceProvider` service providers
  * creates the `ArticleRepository` and `Cart` objects
  * defines the routes and maps them to the twig views

### Front Controller
The front controller `web/index.php` runs the Application.


## Support

Please visit the [Gitlab project page](https://gitlab.com/rodacker/cart-example) and 
use the [issue tracker](https://gitlab.com/rodacker/cart-example/issues) for any feedback
or issues you may have. 
